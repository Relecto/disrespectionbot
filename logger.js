module.exports = (ctx, next) => {
    console.log(
        `[ACCESS] ${new Date().toISOString()} ${ctx.from.username} ${ctx.updateSubTypes}: ${JSON.stringify(ctx.message)}`
    )
    return next()
}