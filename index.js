const Telegraf = require('telegraf')

const { spawn } = require('child_process')
const request = require('request')
const fs = require('fs')

const conf = {
    BOT_TOKEN: process.env.BOT_TOKEN,
    MP3_PATH: process.env.MP3_PATH
}

const bot = new Telegraf(conf.BOT_TOKEN)

bot.use(require('./logger'))

bot.start(ctx => {
    return ctx.reply(`Hey! I can convert voice messages to mp3.\nJust send me a voice message and I will convert it to mp3 for you!`)
})

bot.on('voice', async ctx => {
    await ctx.replyWithChatAction('upload_audio')

    let link = await ctx.telegram.getFileLink(ctx.message.voice.file_id)
    let ogg = request(link)

    let ffmpeg = spawn('ffmpeg', ['-i', 'pipe:0', '-f', 'mp3', 'pipe:1'])

    ogg.pipe(ffmpeg.stdin)

    if (conf.MP3_PATH) {
        ogg.pipe(fs.createWriteStream(`${conf.MP3_PATH}/${ctx.message.message_id}.ogg`))
        ffmpeg.stdout.pipe(fs.createWriteStream(`${conf.MP3_PATH}/${ctx.message.message_id}_converted.mp3`))
    }

    ctx.replyWithAudio({
        source: ffmpeg.stdout
    })
})

bot.catch((err, ctx) => {
    console.log(`[ERROR] ${ctx.updateType}`, err)
})

bot.launch()