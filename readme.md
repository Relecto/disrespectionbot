# Disrespection Bot

This bot uses ffmpeg to convert voice messages to MP3.

Try it at https://t.me/disrespectionbot or run it yourself.

## Running 
If you don't have Telegram Bot API token, talk to [@BotFather](https://t.me/BotFather)

### Docker 
```bash
git clone https://gitlab.com/Relecto/disrespectionbot
cd disrespectionbot

docker build -t disrespectionbot .

docker run -d \
    -e BOT_TOKEN=$YOUR_BOT_TOKEN \
    disrespectionbot
```

### Linux/MacOS/Windows

Make sure you have ffmpeg installed. 

```bash
git clone https://gitlab.com/Relecto/disrespectionbot
cd disrespectionbot
npm install
BOT_TOKEN=$YOUR_BOT_TOKEN npm start
```

### Environment variables:

| Variable  | Description                                                            |
|-----------|------------------------------------------------------------------------|
| BOT_TOKEN | API Token you got from @BotFather                                      |
| MP3_PATH  | If set, audio messages will be saved to this path. Used for debugging. |